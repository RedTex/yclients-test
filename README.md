# yclients-test

> Тестовое задание yclients

## Команды

``` bash
# установить зависимости
npm install

# запустить сервер разработки по адресу localhost:4200
npm run dev

# собрать бандл в директорию dist
npm run build
```
