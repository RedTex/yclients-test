const path = require('path')
const formatter = require('eslint-friendly-formatter')
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')

function resolve(dir) {
  return path.join(__dirname, '..', dir)
}

function addCacheLoader(loaders) {
  if (process.env.NODE_ENV === 'production') {
    return loaders
  } else {
    return [{
      loader: 'cache-loader',
      options: {
        cacheDirectory: resolve('node_modules/.cache/cache-loader')
      }
    }].concat(loaders)
  }
}

module.exports = {
  entry: {
    app: './src/main.ts'
  },
  resolve: {
    extensions: ['.ts', '.js', '.vue', '.json'],
    alias: {
      'src': path.resolve(__dirname, '../src')
    }
  },
  module: {
    rules: [
      {
        test: /\.(js|vue)$/,
        enforce: 'pre',
        include: [resolve('src')],
        use: addCacheLoader([{
          loader: 'eslint-loader',
          options: {
            formatter: require('eslint-friendly-formatter')
          }
        }])
      },
      {
        test: /\.(ts)$/,
        enforce: 'pre',
        include: [resolve('src')],
        use: addCacheLoader([{
          loader: 'tslint-loader'
        }])
      },
      {
        test: /\.vue$/,
        use: addCacheLoader([{
          loader: 'vue-loader',
          options: {
            transformToRequire: {
              video: 'src',
              source: 'src',
              img: 'src',
              image: 'xlink:href'
            }
          }
        }])
      },
      {
        test: /\.js$/,
        use: addCacheLoader(['babel-loader']),
        include: [resolve('src')]
      },
      {
        test: /\.ts$/,
        use: addCacheLoader([
          'babel-loader',
          {
            loader: 'ts-loader',
            options: {
              transpileOnly: true,
              appendTsSuffixTo: [/\.vue$/]
            }
          }
        ]),
        include: [resolve('src')]
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        include: [resolve('mode_modules')],
        use: addCacheLoader([{
          loader: 'file-loader',
          options: {
            name: '/static/[path][name].[ext]'
          }
        }])
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: '/static/fonts/[name].[hash:7].[ext]'
          }
        }]
      },
      {
        test: /\.css$/,
        use: [
          'css-loader'
        ]
      },
      {
        test: /\.stylus$/,
        use: [
          'css-loader', 'stylus-loader'
        ]
      }
    ]
  },
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      cacheGroups: {
        // split vendor js into its own file
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor',
          chunks: 'all'
        }
      }
    }
  },
  plugins: [
    new FriendlyErrorsPlugin(),
    new ForkTsCheckerWebpackPlugin({
      // vue: true,
      tslint: true
    })
  ]
}
