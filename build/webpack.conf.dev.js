const webpack = require('webpack')
const merge = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const webpackConfigBase = require('./webpack.conf.base')
const config = require('../config').dev

// add hot-reload related code to entry chunks
Object.keys(webpackConfigBase.entry).forEach(function (name) {
  webpackConfigBase.entry[name] = ['./build/dev-client'].concat(webpackConfigBase.entry[name])
})

module.exports = merge(
  webpackConfigBase,
  {
    devServer: {
      inline: true,
      port: config.port
    },
    output: {
      path: config.assetsRoot,
      publicPath: config.assetsPublicPath,
      filename: '[name].js'
    },
    resolve: {
      alias: {vue: 'vue/dist/vue.esm.js'}
    },
    mode: 'development',
    // '#eval' is faster for development
    devtool: 'source-map',
    plugins: [
      new webpack.DefinePlugin({
        'process.env': config.env
      }),
      new webpack.HotModuleReplacementPlugin(),
      new HtmlWebpackPlugin({
        filename: 'index.html',
        template: 'index.html',
        // favicon: 'src/assets/images/favicon.ico',
        inject: true
      })
    ]
  }
)
