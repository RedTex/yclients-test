const webpack = require('webpack')
const merge = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin')

const webpackConfigBase = require('./webpack.conf.base')
const config = require('../config').prod

module.exports = merge(
  webpackConfigBase,
  {
    mode: 'production',
    devtool: config.productionSourceMap ? '#source-map' : false,
    output: {
      path: config.assetsRoot,
      filename: 'static/js/[name].[chunkhash].js',
      chunkFilename: 'static/js/[id].[chunkhash].js'
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env': config.env
      }),
      new MiniCssExtractPlugin({
        filename: 'static/css/[name].[contenthash].css'
      }),
      new OptimizeCSSPlugin({
        cssProcessorOptions: {safe: true}
      }),
      new HtmlWebpackPlugin({
        filename: config.index,
        template: 'index.html',
        inject: true,
        minify: {
          removeComments: true,
          collapseWhitespace: true,
          removeAttributeQuotes: true
        },
        favicon: 'src/assets/images/favicon.ico',
        // necessary to consistently work with multiple chunks via CommonsChunkPlugin
        chunksSortMode: 'dependency'
      }),
      // keep module.id stable when vendor modules does not change
      new webpack.HashedModuleIdsPlugin()
    ]
  }
)
