import '@fortawesome/fontawesome-free/js/all'

import Vue from 'vue'
import main from './main.vue'

const Main = Vue.extend(main)

/* eslint-disable no-new */
new Main({el: '#app'})
/* eslint-enable no-new */
