import Puzzle from './Puzzle'

/**
 * Начальное положение паззлов игры.
 * @type {number[]}
 */
const initialData = [
  1,2,3,4,5,6,7,8,9,10,11,12,13,15,14
]

/**
 * Контейнер паззлов игры пятнашки.
 */
export default class Container {
  public readonly puzzles: Array<Puzzle> = []
  public steps: number = 0
  private freeIndex: number = 15

  constructor() {
    initialData.forEach((item: number, index: number) => {
      this.puzzles.push(new Puzzle(this, item, index))
    })
  }

  /**
   * Проверка возможности перемещения паззла.
   * @param {Puzzle} item - паззл
   * @returns {boolean}
   */
  private isAvailable(item: Puzzle): boolean {
    const length = Math.abs(item.index - this.freeIndex)

    return length === 1 || length === 4
  }

  /**
   * Изменить положение паззла.
   * Перед изменением производится проверка на возможность этого изменения.
   * @param {Puzzle} item
   */
  change(item: Puzzle): void {
    if (this.isAvailable(item)) {
      const index = item.index

      item.index = this.freeIndex
      this.freeIndex = index

      this.steps++
    }
  }

  /**
   * Проверка правильной расстановки паззлов.
   * @returns {boolean}
   */
  isValid(): boolean {
    return this.puzzles.every((item: Puzzle) => item.isValid())
  }

  /**
   * Удаление данных контейнера.
   * (необходимо чтобы из-за большой связности объектов, которую создает Vue, элементы не оставались в памяти)
   */
  destroy(): void {
    this.puzzles.forEach(puzzle => puzzle.destroy())

    Object.keys(this).forEach(key => {
      delete this[key]
    })
  }
}
