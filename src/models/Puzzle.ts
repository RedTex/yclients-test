import Container from './Container'

/**
 * Паззл игры пятнашки.
 */
export default class Puzzle {
  constructor(private container: Container, public name: number, public index: number) {}

  /**
   * Изменить положение паззла.
   */
  change(): void {
    this.container.change(this)
  }

  /**
   * Проверка правильности расположения паззла.
   * @returns {boolean}
   */
  isValid(): boolean {
    return this.index === this.name
  }

  /**
   * Удалить все свойства экзепляра.
   */
  destroy(): void {
    Object.keys(this).forEach(key => {
      delete this[key]
    })
  }
}
