const path = require('path')

module.exports = {
  prod: {
    env: require('./env.prod'),
    index: path.resolve(__dirname, '../dist/index.html'),
    productionSourceMap: true,
    assetsRoot: path.resolve(__dirname, '../dist')
  },
  dev: {
    env: require('./env.dev'),
    port: 4200,
    autoOpenBrowser: true,
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    assetsRoot: path.resolve(__dirname, '../dist')
  }
}
